#!/usr/bin/env python

from distutils.core import setup

setup(
    name='dojo-ftp',
    version='0.0.1',
    description='Dojo FTP drop source adapter',
    author='Steven Normore',
    author_email='steven@dataup.io',
    url='https://dojo.dataup.io/',
    packages=['dojo_ftp', ],
    install_requires=[]
)
